﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    private CreateObj _createObj;
    // Start is called before the first frame update
    void Start()
    {
        _createObj = CreateObj.Instance;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _createObj.Spawn(Point.point1, "a");
        }
    }
}
