﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CreateObj : MonoBehaviour
{
    private static CreateObj mInstance;

    public static CreateObj Instance
    {
        get
        {
            return mInstance;
        }
    }

    [SerializeField]
    private List<point> points = new List<point>();

    private GameMaster _gameMaster;

    private void Awake()
    {
        //シングルトンなオブジェクトにするための処理
        if (mInstance == null)
        {
            mInstance = this;
            //DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Debug.LogError(this.gameObject.name + "についている" + this + "を削除しました。");
        }
    }

    private void Start()
    {
        _gameMaster = GameMaster.Instance;
    }

    /// <summary>
    /// オブジェクトを生成します。
    /// </summary>
    /// <param name="point">生成するポイント</param>
    /// <param name="name">オブジェクトの名前</param>
    public void Spawn(Point point, string name)
    {
        var p = points.FirstOrDefault(n => n.GetPoint() == point);
        var obj = _gameMaster.GetSpawnDataBase().GetSpawnObj(name).GetObj();
        var newObj = Instantiate(obj, p.GetTra());
        p.SetSpawnObj(newObj);
    }

    /// <summary>
    /// スポーンさせたオブジェクトを削除します。
    /// </summary>
    /// <param name="point">削除したいスポーン</param>
    public void DestroyObj(Point point)
    {
        Destroy(points.FirstOrDefault(n => n.GetPoint() == point).GetSpawnObj());
    }
}

[System.Serializable]
public class point
{
    [SerializeField]
    private Transform tra;
    [SerializeField]
    private Point pt;

    private GameObject _spaenObj;  //ポイントに生成したオブジェクトを入れておく

    public Transform GetTra()
    {
        return tra;
    }

    public Point GetPoint()
    {
        return pt;
    }

    public void SetSpawnObj(GameObject spawnObj)
    {
        this._spaenObj = spawnObj;
    }

    public GameObject GetSpawnObj()
    {
        return _spaenObj;
    }
}

public enum Point
{
    point1 = 1,
    point2 = 2,
    point3 = 3,
    point4 = 4,
    point5 = 5,
    point6 = 6,
    point7 = 7,
    point8 = 8,
    point9 = 9,
    point10 = 10,
    point11 = 11,
    point12 = 12,
    point13 = 13,
    point14 = 14,
    point15 = 15,
    point16 = 16
}
