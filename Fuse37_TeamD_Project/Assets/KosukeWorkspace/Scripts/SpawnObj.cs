﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "CreateSpawnObj", fileName = "NewSpawnObj")]
public class SpawnObj : ScriptableObject
{
    [SerializeField]
    private GameObject objPrefab;  //スポーンさせたいオブジェクトをアタッチ
    [SerializeField]
    private string objName;  //オブジェクトの名前を設定
    [SerializeField]
    private int cost;  //スポーンさせるために必要なコストを設定


    /// <summary>
    /// オブジェクトのPrefabを取得できます。
    /// </summary>
    /// <returns>Prefab</returns>
    public GameObject GetObj()
    {
        return objPrefab;
    }

    /// <summary>
    /// オブジェクトの名前を取得できます。
    /// </summary>
    /// <returns>名前</returns>
    public string GetName()
    {
        return objName;
    }

    /// <summary>
    /// オブジェクトのスポーンコストを取得できます。
    /// </summary>
    /// <returns>コスト</returns>
    public int GetCost()
    {
        return cost;
    }
}
