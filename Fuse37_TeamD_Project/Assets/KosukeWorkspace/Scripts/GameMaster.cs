﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMaster : MonoBehaviour
{
    private static GameMaster mInstance;

    public static GameMaster Instance
    {
        get
        {
            return mInstance;
        }
    }

    [SerializeField]
    private SpawnObjDataBase spawnObjDataBase;  //スポーンデータベースのアタッチ

    private int _player1Score;  //プレイヤー１のスコア
    private int _player2Score;  //プレイヤー２のスコア

    private void Awake()
    {
        //シングルトンなオブジェクトにするための処理
        if(mInstance == null)
        {
            mInstance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Debug.LogError(this.gameObject.name + "についている" + this + "を削除しました。");
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// スポーンするオブジェクトのデータベースを取得できます。
    /// </summary>
    /// <returns>スポーンデータベース</returns>
    public SpawnObjDataBase GetSpawnDataBase()
    {
        return spawnObjDataBase;
    }


    /// <summary>
    /// プレイヤー１のスコアを取得できます。
    /// </summary>
    /// <returns>プレイヤー１のスコア</returns>
    public int GetPlayer1Score()
    {
        return _player1Score;
    }

    /// <summary>
    /// プレイヤー２のスコアを取得できます。
    /// </summary>
    /// <returns>プレイヤー２のスコア</returns>
    public int GetPlayer2Score()
    {
        return _player2Score;
    }

    /// <summary>
    /// プレイヤー１のスコアを設定できます。
    /// </summary>
    /// <param name="score">プレイヤー１のスコア</param>
    public void SetPlayer1Score(int score)
    {
        _player1Score = score;
    }
    
    /// <summary>
    /// プレイヤー２のスコアを設定できます。
    /// </summary>
    /// <param name="score">プレイヤー２のスコア</param>
    public void SetPlayer2Score(int score)
    {
        _player2Score = score;
    }

    /// <summary>
    /// シーンを移動します。
    /// </summary>
    /// <param name="sceneName">移動したいシーン名</param>
    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
}
