﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultUiController : MonoBehaviour
{
    [SerializeField]
    private Text blueScoreText;
    [SerializeField]
    private Text redScoreText;

    private GameMaster _gameMaster;

    // Start is called before the first frame update
    void Start()
    {
        _gameMaster = GameMaster.Instance;

        blueScoreText.text = _gameMaster.GetPlayer1Score().ToString();
        redScoreText.text = _gameMaster.GetPlayer2Score().ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TitleBack()
    {
        _gameMaster.LoadScene("Title");
    }
}
