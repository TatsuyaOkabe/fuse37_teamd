﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleUiController : MonoBehaviour
{
    private GameMaster _gameMaster;

    // Start is called before the first frame update
    void Start()
    {
        _gameMaster = GameMaster.Instance;
    }

    public void MainLoad()
    {
        _gameMaster.LoadScene("Stage");
    }
}
