﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(menuName = "CreateSpawnObjDataBase", fileName = "NewSpawnObjDataBase")]
public class SpawnObjDataBase : ScriptableObject
{
    [SerializeField]
    private List<SpawnObj> spawnObjs = new List<SpawnObj>();

    /// <summary>
    /// 名前からそれに一致するオブジェクトのデータを渡します。
    /// </summary>
    /// <param name="objName">オブジェクト名</param>
    /// <returns>取得したSpawnObj情報</returns>
    public SpawnObj GetSpawnObj(string objName)
    {
        return spawnObjs.FirstOrDefault(n => n.GetName() == objName);
    }

    /// <summary>
    /// すべてのオブジェクトの名前を取得できます。
    /// </summary>
    /// <returns>データベースにあるオブジェクトの名前</returns>
    public List<string> GetSpawnObjNames()
    {
        List<string> objNames = new List<string>();
        foreach(SpawnObj spawnObj in spawnObjs)
        {
            objNames.Add(spawnObj.GetName());
        }

        return objNames;
    }
}
