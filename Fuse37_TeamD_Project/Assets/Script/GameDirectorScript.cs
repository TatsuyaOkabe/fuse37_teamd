﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class GameDirectorScript : MonoBehaviour
{
    public List<GameObject> m_playerObejct;
    public List<GameObject> m_enemyObject;
    public List<GameObject> m_baseObject;

    //ゲームの時間
    public float m_gameTime = 0;

    //ゲームの終了時間
    float m_endTime = 120f;

    //ゲッター
    public List<GameObject> playerObjectData
    { get { return this.m_playerObejct; } }

    public List<GameObject> enemyObjectData
    { get { return this.m_enemyObject; } }

    public List<GameObject> baseObjectData
    { get { return this.m_baseObject; } }

    // Start is called before the first frame update
    void Start()
    {
        SearchGameObject();
    }

    // Update is called once per frame
    void Update()
    {
        TimeController();
    }

    void TimeController()
    {
        if (m_gameTime > m_endTime)
        {
            Debug.Log("ゲームセット");
            //SceneManager.LoadScene("");
        }
        else
        {
            var delta = Time.deltaTime;
            m_gameTime += delta;
        }
    }

    public void SearchDesObject()
    {

    }

    void SearchGameObject()
    {
        foreach(GameObject obj in UnityEngine.Object.FindObjectsOfType(typeof(GameObject)))
        {
            if (obj.activeInHierarchy)
            {
                SortingObject(obj);
            }
        }
    }

    void SortingObject(GameObject obj)
    {
        var objtag = obj.tag;

        switch (objtag)
        {
            case "Player":
                m_playerObejct.Add(obj);
                break;

            case "Enemy":
                m_enemyObject.Add(obj);
                break;

            case "Base":
                m_baseObject.Add(obj);
                break;

            default:
                break;
        }
    }
}
