﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    GameObject m_playerObj;
    PlayerScript m_playerScript;
    Vector3 m_defCameraPos;

    // Start is called before the first frame update
    void Start()
    {
        m_playerObj = GameObject.Find("Player");
        m_playerScript = m_playerObj.GetComponent<PlayerScript>();
        m_defCameraPos = this.gameObject.transform.position;
        GetPosition();
    }

    // Update is called once per frame
    void Update()
    {
        GetPosition();
    }

    void GetPosition()
    {
        var addPos = m_playerScript.addPos;
        this.transform.position += addPos;
    }
}
