﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectBaseScript : MonoBehaviour
{
    public float m_hp = 0;
    public float m_power = 1;
    public bool m_desFlag = false;

    public float hp { get { return this.m_hp; } }
    public float power { get { return this.m_power; } }
    public bool desFlag { get { return this.m_desFlag; } }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    uint FlagTrue(uint flags, uint setNum)
    {
        return flags | setNum;
    }

    uint FlagFalse(uint flags, uint setNum)
    {
        return flags & ~(setNum);
    }
}
