﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseAreaScript : MonoBehaviour
{
    enum FlagName
    {
        unknown = 0x001,
        player = 0x002,
        enemy = 0x004,
    }

    public float m_hp = 0;
    public float m_maxHp = 0;
    uint m_baseTypeFlag = 0x000;
    uint m_attackFlag = 0x000;
    string m_tagName;

    // Start is called before the first frame update
    void Start()
    {
        m_hp = m_maxHp;
        m_baseTypeFlag = (uint)FlagName.unknown;
    }

    // Update is called once per frame
    void Update()
    {
        DamageBase();
        GetBase();
    }

    //プレイヤーまたはエネミーのどちらか片方がいるときだけHPを減らす
    void DamageBase()
    {
        var flag = (((m_attackFlag & (uint)FlagName.player) != 0) &&
                   ((m_attackFlag & (uint)FlagName.enemy) != 0)) ||
                   (((m_attackFlag & (uint)FlagName.player) == 0) &&
                   ((m_attackFlag & (uint)FlagName.enemy) == 0));

        if (!flag)
        {
            var delta = Time.deltaTime;
            m_hp -= 1 * delta;
            Debug.Log(flag);
        }
    }

    //拠点のHPがゼロ以下になった時の処理
    void GetBase()
    {
        if (m_hp <= 0)
        {
            //拠点のタイプを初期化する
            m_baseTypeFlag = 0x000;

            //最後に攻撃したチームが拠点を取得する
            switch (m_attackFlag)
            {
                case (uint)FlagName.unknown:
                    break;

                case (uint)FlagName.player:
                    m_baseTypeFlag |= (uint)FlagName.player;                
                    break;

                case (uint)FlagName.enemy:
                    m_baseTypeFlag |= (uint)FlagName.enemy;
                    break;

                default:
                    break;
            }

            SelectType();
            m_hp = m_maxHp;
        }
    }

    //拠点の見た目変更
    void SelectType()
    {
        if ((m_baseTypeFlag & (uint)FlagName.unknown) != 0)
        {
            Debug.Log("未所属");
        }else

        if ((m_baseTypeFlag & (uint)FlagName.player) != 0)
        {
            Debug.Log("プレイヤー");
        }else

        if ((m_baseTypeFlag & (uint)FlagName.enemy) != 0)
        {
            Debug.Log("エネミー");
        }
    }

    //
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            m_attackFlag |= (uint)FlagName.player;
        }

        if (other.gameObject.tag == "Enemy")
        {
            m_attackFlag |= (uint)FlagName.enemy;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            m_attackFlag &= ~((uint)FlagName.player);
        }

        if (other.gameObject.tag == "Enemy")
        {
            m_attackFlag &= ~((uint)FlagName.enemy);
        }
    }
}
