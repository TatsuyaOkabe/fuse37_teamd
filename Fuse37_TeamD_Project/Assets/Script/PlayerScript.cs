﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;

public class PlayerScript : ObjectBaseScript
{
    protected enum FlagName
    {
        enemyHit = 0x001,
    }

    public uint m_hitFlag = 0x000;

    Vector3 playerPos = new Vector3(0, 0, 0);
    public Vector3 m_addPos = new Vector3(0, 0, 0);

    public float m_speed = 0;

    public Vector3 addPos
    {
        get { return this.m_addPos; }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        MovePlayer();
        IsHitEnemy();
        DedPlayer();
    }

    //プレイヤーの動き
    void MovePlayer()
    {
        if (Input.anyKey)
        {
            if(Input.GetKey(KeyCode.UpArrow))
            {
                m_addPos = new Vector3(0, 0, m_speed);
            }

            if (Input.GetKey(KeyCode.DownArrow))
            {
                m_addPos = new Vector3(0, 0, -m_speed);
            }

            if (Input.GetKey(KeyCode.RightArrow))
            {
                m_addPos = new Vector3(m_speed, 0, 0);
            }

            if (Input.GetKey(KeyCode.LeftArrow))
            {
                m_addPos = new Vector3(-m_speed, 0, 0);
            }

            this.transform.position += m_addPos;
        }
        else
        {
            m_addPos = new Vector3(0, 0, 0);
        }
    }


    void IsHitEnemy()
    {
        if ((m_hitFlag & (uint)FlagName.enemyHit) != 0)
        {
            var delta = Time.deltaTime;
            m_hp -= delta;
        }
    }

    void DedPlayer()
    {
        if (m_hp > 0)
        {
            Debug.Log("ゲームオーバー");
            //SceneManager.LoadScene("");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            m_hitFlag |= (uint)FlagName.enemyHit;
        }
    }
}
