﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : ObjectBaseScript
{
    protected enum FlagName
    {
        hitEnemy = 0x001,
    }

    public uint m_hitFlag = 0x000;
    public string m_enemyTag;

    // Start is called before the first frame update
    void Start()
    {
        CheckTag();
    }



    // Update is called once per frame
    void Update()
    {
        
    }

    private void CheckTag()
    {
        if(this.gameObject.tag == "Player")
        {
            m_enemyTag = "Enemy";
        }
        else
        {
            m_enemyTag = "Player";
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == m_enemyTag)
        {
            m_hitFlag |= (uint)FlagName.hitEnemy;
        }
    }


}
