﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PushButton : MonoBehaviour
{
    public AudioClip Startkyoku;
    AudioSource audioSource;

    public int m_buttonNum;
    public bool m_pushButton;
    public int buttonNum
    { get { return m_buttonNum; } }

    // Start is called before the first frame update
    void Start()
    {
        audioSource = this.gameObject.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnClick()
    {
        if (!m_pushButton)
        {
            audioSource.clip = Startkyoku;
            audioSource.PlayOneShot(Startkyoku);
            m_pushButton = true;

            //ここでデータを送る
            Debug.Log(m_buttonNum.ToString());
        }
    }
}
